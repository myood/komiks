from komiks.file import get_file_type


class Loader(object):

    loaders = []
    image_factory = None

    def __init__(self, **kwargs):
        if 'loaders' in kwargs:
            self.loaders = kwargs['loaders']
        if 'image_factory' in kwargs:
            self.image_factory = kwargs['image_factory']

    def load_all(self, file_path, receiver):
        loader = self._get_loader(file_path)
        if loader is None:
            receiver(None, comic=file_path)
            return
        self._load(self.image_factory, loader, receiver)

    def _get_loader(self, file_path):
        compatible_loaders = [l for l in self.loaders if get_file_type(file_path) in l.supported_file_types()]
        if len(compatible_loaders) == 0:
            return None
        return compatible_loaders[0].create_instance(file_path)

    @staticmethod
    def _load(factory, comic, receiver):
        for page in comic.list_files():
            receiver(
                factory.create(comic=comic, page=page), 
                comic=comic.file_path,
                page=page)