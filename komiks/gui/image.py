import io

from kivy.core.image import Image as CoreImage
from kivy.uix.image import Image as KivyImage

from komiks.file import get_file_type


class Image(KivyImage):
    comic = ''
    page = ''

    def __init__(self, **kwargs):
        super(Image, self).__init__(**kwargs)
        if 'comic' in kwargs and 'page' in kwargs:
            self.load(kwargs['comic'], kwargs['page'])

    def load(self, comic, page):
        data = io.BytesIO(comic.read(page))
        core_image = CoreImage(data, ext=get_file_type(page), filename=page)
        self.texture = core_image.texture

        self.comic = comic.file_path
        self.page = page
