from kivy.uix.gridlayout import GridLayout as KivyGridLayout
from kivy.uix.scrollview import ScrollView as KivyScrollView


class Viewer(KivyScrollView):
    view_mode = 'resize to window'

    def __init__(self, **kwargs):
        super(Viewer, self).__init__(**kwargs)
        self.layout = KivyGridLayout(cols=1, size_hint=(None, None))
        self.layout.bind(minimum_height=self.layout.setter('height'))
        self.add_widget(self.layout)

    def add_page(self, widget, **kwargs):
        self._setup_page(widget)
        self._resize_page(widget)
        self.layout.add_widget(widget)

    def scroll_to(self, comic, page):
        widget = self._find_widget_for(comic, page)
        if widget:
            KivyScrollView.scroll_to(self, widget)

    @staticmethod
    def _setup_page(widget):
        widget.keep_ratio = True
        widget.allow_stretch = True
        widget.size_hint = (None, None)

    def _resize_page(self, widget):
        if self.view_mode == 'resize to width':
            widget.size = (self.width, (self.width / widget.image_ratio))
        if self.view_mode == 'resize to window':
            widget.size = self.size

    def on_size(self, instance, value):
        [self._resize_page(child) for child in self.layout.children]

    def _find_widget_for(self, comic, page):
        return next((x for x in self.layout.children if (x.comic == comic and x.page == page)), [None])
