import zipfile

from komiks.file import is_directory


class Cbz(object):

    def __init__(self, file_path):
        self.file_path = file_path

    def list_files(self):
        with zipfile.ZipFile(self.file_path, 'r', ) as my_zip:
            everything = my_zip.namelist()
            files_only = [f for f in everything if not is_directory(f)]
            return files_only

    def read(self, filename_in_archive):
        with zipfile.ZipFile(self.file_path, 'r') as my_zip:
            return my_zip.read(filename_in_archive)

    @staticmethod
    def supported_file_types():
        return ['cbz']

    @staticmethod
    def create_instance(file_path):
        return Cbz(file_path)
