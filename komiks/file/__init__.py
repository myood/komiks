from os.path import splitext as get_extension


def get_file_type(filename):
    base_path, extension = get_extension(filename)
    file_type = extension[1:]
    return file_type


def is_directory(filename):
    return filename.endswith('\\') or filename.endswith('/')
