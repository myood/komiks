from kivy.uix.image import Image

import test.data.cbz as test_data
from komiks.gui.viewer import Viewer
from test.kivy_gui_test import GraphicUnitTest


class TestViewerGui(GraphicUnitTest):
    def build_up_viewer_from_test_data(self):
        viewer = Viewer()
        for i in range(len(test_data.file_paths_in_archive)):
            img_fake = Image(source=test_data.extracted_files_full_path[i])
            img_fake.comic = test_data.archive_path
            img_fake.page = test_data.file_paths_in_archive[i]
            viewer.add_page(img_fake, comic=img_fake.comic, page=img_fake.page)
            self.assertEqual(img_fake, viewer._find_widget_for(img_fake.comic, img_fake.page))
        return viewer

    def test_fit_to_window(self):
        viewer = self.build_up_viewer_from_test_data()
        viewer.view_mode = 'resize to window'
        self.render(viewer)

    def test_fit_to_width(self):
        viewer = self.build_up_viewer_from_test_data()
        viewer.view_mode = 'resize to width'
        self.render(viewer)


if __name__ == '__main__':
    from kivy.app import App as KivyApp


    class TestApp(KivyApp):

        def build(self):
            return Viewer()

        def on_start(self):
            for i in range(len(test_data.file_paths_in_archive)):
                img_fake = Image(source=test_data.extracted_files_full_path[i])
                img_fake.comic = test_data.archive_path
                img_fake.page = test_data.file_paths_in_archive[i]
                self.root.add_page(img_fake, comic=test_data.archive_path, page=test_data.file_paths_in_archive[i])
            self.root.scroll_to(test_data.archive_path, test_data.file_paths_in_archive[1])

    TestApp().run()
