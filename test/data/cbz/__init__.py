import os

directory = os.path.join('data', 'cbz')
folder_name = 'folder'
extracted_folder_path = os.path.join(directory, folder_name)
archive_file_type = 'cbz'
archive_file_extension = '.' + archive_file_type
archive_path = extracted_folder_path + archive_file_extension
windows_archive_path = extracted_folder_path + '_win.cbz'
__extracted_file_names__ = os.listdir(extracted_folder_path)
__archived_files_not_normalized__ = [os.path.join(folder_name, filename) for filename in __extracted_file_names__]
extracted_files_full_path = [os.path.join(extracted_folder_path, file) for file in __extracted_file_names__]
file_paths_in_archive = [os.path.normpath(f) for f in __archived_files_not_normalized__]
