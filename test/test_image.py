import sys
import unittest

from kivy.core.image import Image as CoreImage
from kivy.uix.widget import Widget

import test.data.cbz as test_data
from komiks.file.cbz import Cbz
from komiks.gui.image import Image
from test.kivy_gui_test import GraphicUnitTest

reference_image = test_data.extracted_files_full_path[0]


class TestImage(GraphicUnitTest):

    def setUp(self):
        super(TestImage, self).setUp()
        self.comic = Cbz(test_data.archive_path)
        files = self.comic.list_files()
        self.test_image = files[0]
        self.expected = CoreImage(reference_image).texture

    @unittest.skipIf(sys.platform.startswith('win'), 'Widget c-tor does not accept non-standards kwargs on Windows')
    def test_ctor(self):
        im = Image(comic=self.comic, page=self.test_image)
        self.actual = im.texture

    def test_load(self):
        im = Image()
        im.load(self.comic, self.test_image)
        self.actual = im.texture

    def tearDown(self, fake=False):
        e = self.expected
        a = self.actual
        self.assertEqual(e.size, a.size)
        self.assertEqual(e.bufferfmt, a.bufferfmt)
        self.assertEqual(e.colorfmt, a.colorfmt)
        self.assertEqual(e.mag_filter, a.mag_filter)
        self.assertEqual(e.min_filter, a.min_filter)
        self.assertEqual(e.mipmap, a.mipmap)
        self.assertEqual(e.wrap, a.wrap)
        self.assertEqual(e.uvpos, a.uvpos)
        self.assertEqual(e.uvsize, a.uvsize)
        self.assertEqual(e.pixels, a.pixels)


if __name__ == '__main__':
    from kivy.app import App

    class TestApp(App):
        def build(self):
            return Widget()

        def on_start(self):
            im = Image(source=reference_image, allow_stretch=True, mipmap=True, pos=self.root.pos, size=self.root.size)
            self.root.add_widget(im)

    TestApp().run()
