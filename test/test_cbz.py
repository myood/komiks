import os
import unittest as ut

import test.data.cbz as test_data
from komiks.file.cbz import Cbz


class TestCbz(ut.TestCase):
    archive = test_data.archive_path

    def process_ls(self, archive_path):
        expected = set(test_data.file_paths_in_archive)
        actual = set([os.path.normpath(f) for f in Cbz(archive_path).list_files()])
        self.assertEqual(expected, actual)

    def test_ls(self):
        self.process_ls(test_data.archive_path)

    def test_ls_win(self):
        self.process_ls(test_data.windows_archive_path)

    def test_read(self):
        comic_file = Cbz(test_data.archive_path)
        for filename in comic_file.list_files():
            with open(os.path.join(test_data.directory, os.path.normpath(filename)), 'rb') as reference:
                expected = reference.read()
                actual = comic_file.read(filename)
                self.assertEqual(expected, actual)

    def test_supported_file_type(self):
        self.assertEqual([test_data.archive_file_type], Cbz.supported_file_types())

    def test_create(self):
        self.assertEqual(type(Cbz('')), type(Cbz.create_instance('some_file_path')))
