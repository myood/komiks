import os
import unittest as ut

from komiks.file import get_file_type
from komiks.file import is_directory


class TestCommon(ut.TestCase):

    def test_get_file_type(self):
        self.assertEqual('jpg', get_file_type('file.jpg'))
        self.assertEqual('cbz', get_file_type(os.path.join('path_to', 'file.cbz')))

    def test_is_directory(self):
        self.assertFalse(is_directory(os.path.join('folder', 'file')))
        self.assertFalse(is_directory('file'))
        self.assertTrue(is_directory('folder/'))
        self.assertTrue(is_directory('folder\\'))
