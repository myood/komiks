import unittest as ut
from unittest.mock import Mock
from unittest.mock import call as Call

import test.data.cbz as test_data
from komiks.core.loader import Loader
from komiks.file.cbz import Cbz as ComicProduction


class TestLoaderMocked(ut.TestCase):

    def setUp(self):
        self.files_in_archive = ComicProduction(test_data.archive_path).list_files()
        self.receiver_mock = Mock()
        self.image_mock = Mock()
        self.image_factory_mock = Mock()
        self.image_factory_mock.create = Mock(return_value=self.image_mock)
        self.comic_mock = Mock()
        self.comic_mock.supported_file_types = Mock(return_value=['type1', 'type2'])

        self.loader = Loader(loaders=[self.comic_mock, ComicProduction], image_factory=self.image_factory_mock)

    def test_unsupported(self):
        unsupported = '/unsupported/file.format'
        self.loader.load_all(unsupported, self.receiver_mock)
        self.receiver_mock.assert_called_once_with(None, comic=unsupported)

    def test_load(self):
        self.loader.load_all(test_data.archive_path, self.receiver_mock)
        pages = len(self.files_in_archive)
        self.assertEqual(pages, self.receiver_mock.call_count)
        calls = [self.create_call(i) for i in range(pages)]
        self.receiver_mock.assert_has_calls(calls)

    def test_late_init(self):
        self.loader = Loader()
        self.loader.loaders.append(self.comic_mock)
        self.loader.loaders.append(ComicProduction)
        self.loader.image_factory = self.image_factory_mock
        self.test_load()

    def create_call(self, i):
        return Call(self.image_mock, comic=test_data.archive_path, page=self.files_in_archive[i])
